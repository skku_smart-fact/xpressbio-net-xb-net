# XpressBio Net (XB-Net)

## Introduction
이 프로젝트는 성균관대학교 스마트팩토리융합캡스톤디자인2 과목의 과제물로 제출하기위해 만들어졌습니다.

참고한 논문 소스코드의 출처(https://github.com/westgate458/XB-Net)

## References:
1. Tianqi Guo, Yin Wang, Luis Solorio, and Jan P. Allebach (2021). Training a universal instance segmentation network for live cell images of various cell types and imaging modalities. (Manuscript in preparation)
2. Guo, T., Ardekani, A. M., & Vlachos, P. P. (2019). Microscale, scanning defocusing volumetric particle-tracking velocimetry. Experiments in Fluids, 60(6), 89. [Link](https://link.springer.com/article/10.1007/s00348-019-2731-4)
3. Jun, B. H., Guo, T., Libring, S., Chanda, M. K., Paez, J. S., Shinde, A., ... & Solorio, L. (2020). Fibronectin-expressing mesenchymal tumor cells promote breast cancer metastasis. Cancers, 12(9), 2553. [Link](https://doi.org/10.3390/cancers12092553)
4. Ronneberger, O., Fischer, P., & Brox, T. (2015, October). U-net: Convolutional networks for biomedical image segmentation. In International Conference on Medical image computing and computer-assisted intervention (pp. 234-241). Springer, Cham. [Link](https://arxiv.org/abs/1505.04597)
