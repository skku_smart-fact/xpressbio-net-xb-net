import matplotlib.pyplot as plt
import glob
import os
import sys

input_data_path = "./test_results"

img_list = sorted(glob.glob(os.path.join(input_data_path, '*.tif')))

for image_path in img_list:
    img = plt.imread(image_path)
    imgsh = plt.imshow(img)
    plt.show()